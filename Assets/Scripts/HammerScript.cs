﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class HammerScript : MonoBehaviour
{
    [SerializeField] LayerMask attachedLayer;
    [SerializeField] float hammerRadius, maxDistance = 1f;
    [SerializeField] AudioClip woodSound, stoneSound, metalSound;
    [SerializeField] int woodLayer, stoneLayer, metalLayer;
    AudioSource audio;
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.loop = false;
        audio.spatialBlend = 1;
        audio.playOnAwake = false;
        if (audio.isPlaying) audio.Stop();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (audio.isPlaying) audio.Stop();
        if (collision.gameObject.layer == woodLayer) audio.clip = woodSound;
        if (collision.gameObject.layer == stoneLayer) audio.clip = stoneSound;
        if (collision.gameObject.layer == metalLayer) audio.clip = metalSound;
        audio.Play();
        if (transform.parent)
        {
            GameObject toAttach, mount;
            RaycastHit mountHit;
            toAttach = collision.gameObject;
            if (Physics.SphereCast(transform.position, hammerRadius, transform.forward, out mountHit, maxDistance, attachedLayer))
            {
                if (toAttach != null && toAttach.GetComponent<CustomObjectScript>() && mountHit.transform != null)
                {
                    mount = mountHit.transform.gameObject;
                    CustomObjectScript attachObj = toAttach.GetComponent<CustomObjectScript>(),
                        mountObj = mount.transform.GetComponent<CustomObjectScript>();
                    if (!attachObj.attached) attachObj.AttachTo(mount.transform);
                }
            }
        }
    }
}
