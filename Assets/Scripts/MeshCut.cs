﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshCut
{
    static Plane blade;//плоскость сечения
    static Mesh targetMesh;//меш, который нужно разрезать
    static AdvancedMesh leftSide = new AdvancedMesh();//левая часть
    static AdvancedMesh rightSide = new AdvancedMesh();//правая часть
    static AdvancedMesh.Triangle tempTriangle = AdvancedMesh.NewTriangle();//врем. треугольник
    static List<Vector3> addedVertices = new List<Vector3>();//вершины рассеченных треугольников, образующие края рассечения
    static bool[] isLeftSide = new bool[3];//лежит ли эта точка треуг. слева от плоскости сечения
    static int cutMatSubmesh = 1;//новый submesh, в который будут записаны вершины по краям от рассечения
    static AdvancedMesh.Triangle tempLeftTriangle = AdvancedMesh.NewTriangle();
    static AdvancedMesh.Triangle tempRightTriangle = AdvancedMesh.NewTriangle();
    static AdvancedMesh.Triangle newTriangle = AdvancedMesh.NewTriangle();
    static List<int> usedIndeces = new List<int>();//использованные вершины с краёв
    static List<int> polygonIndeces = new List<int>();//плоскость края отсеченной фигуры

    public static GameObject[] Cut(GameObject target, Vector3 bladePoint, Vector3 normalDirection, Material capMaterial)
    {

        blade = new Plane(target.transform.InverseTransformDirection(-normalDirection),
            target.transform.InverseTransformPoint(bladePoint));

        targetMesh = target.GetComponent<MeshFilter>().mesh;
        //origin.SetMesh(new List<Vector3>(targetMesh.vertices), new List<Vector3>(targetMesh.normals), new List<Vector2>(targetMesh.uv), new List<Vector4>(targetMesh.tangents), new List<List<int>>());

        // two new meshes
        leftSide.Clear();
        rightSide.Clear();
        addedVertices.Clear();


        int ind1, ind2, ind3;

        var mesh_vertices = targetMesh.vertices;
        //var mesh_normals = targetMesh.normals;
        //var mesh_uvs = targetMesh.uv;
        //var mesh_tangents = targetMesh.tangents;
        //if (mesh_tangents != null && mesh_tangents.Length == 0)
        //    mesh_tangents = null;

        // go through the submeshes
        for (int submeshIterator = 0; submeshIterator < targetMesh.subMeshCount; submeshIterator++)
        {

            // Triangles
            var indices = targetMesh.GetTriangles(submeshIterator);

            for (int i = 0; i < indices.Length; i += 3)
            {

                ind1 = indices[i];
                ind2 = indices[i + 1];
                ind3 = indices[i + 2];

                AdvancedMesh.SetTriangle(ref tempTriangle, targetMesh, ind1, ind2, ind3);

                // which side are the vertices on
                isLeftSide[0] = blade.GetSide(mesh_vertices[ind1]);
                isLeftSide[1] = blade.GetSide(mesh_vertices[ind2]);
                isLeftSide[2] = blade.GetSide(mesh_vertices[ind3]);


                // whole triangle
                if (isLeftSide[0] == isLeftSide[1] && isLeftSide[0] == isLeftSide[2])
                {
                    if (isLeftSide[0]) // left side
                        leftSide.AddTriangle(tempTriangle, submeshIterator);
                    else // right side
                        rightSide.AddTriangle(tempTriangle, submeshIterator);
                }
                else
                { // cut the triangle

                    CutFace(ref tempTriangle, submeshIterator);
                }
            }
        }

        // The capping Material will be at the end
        Material[] mats = target.GetComponent<MeshRenderer>().sharedMaterials;
        if (mats[mats.Length - 1].name != capMaterial.name)
        {
            Material[] newMats = new Material[mats.Length + 1];
            mats.CopyTo(newMats, 0);
            newMats[mats.Length] = capMaterial;
            mats = newMats;
        }
        cutMatSubmesh = mats.Length - 1; // for later use

        // cap the opennings
        CapTheCut();


        // Left Mesh
        Mesh left_HalfMesh = leftSide.GetMesh();
        left_HalfMesh.name = targetMesh.name + " Left";

        // Right Mesh
        Mesh right_HalfMesh = rightSide.GetMesh();
        right_HalfMesh.name = targetMesh.name + " Right";

        // assign the game objects

        string tempName = target.name;
        target.name += " Left";
        target.GetComponent<MeshFilter>().mesh = left_HalfMesh;

        GameObject leftSideObj = target;

        GameObject rightSideObj = new GameObject(tempName + " Right", typeof(MeshFilter), typeof(MeshRenderer));
        rightSideObj.transform.position = target.transform.position;
        rightSideObj.transform.rotation = target.transform.rotation;
        rightSideObj.GetComponent<MeshFilter>().mesh = right_HalfMesh;

        if (target.transform.parent != null)
        {
            rightSideObj.transform.parent = target.transform.parent;
        }

        rightSideObj.transform.localScale = target.transform.localScale;


        // assign mats
        leftSideObj.GetComponent<MeshRenderer>().materials = mats;
        rightSideObj.GetComponent<MeshRenderer>().materials = mats;

        return new GameObject[] { leftSideObj, rightSideObj };

    }

    static void CutFace(ref AdvancedMesh.Triangle triangle, int submesh)
    {

        isLeftSide[0] = blade.GetSide(triangle.vertices[0]);
        isLeftSide[1] = blade.GetSide(triangle.vertices[1]);
        isLeftSide[2] = blade.GetSide(triangle.vertices[2]);


        int leftCount = 0;
        int rightCount = 0;

        for (int i = 0; i < 3; i++)
        {
            if (isLeftSide[i])
            { // left

                AdvancedMesh.SetTrianglePoint(ref tempLeftTriangle, leftCount, triangle, i);
                leftCount++;
            }
            else
            { // right

                AdvancedMesh.SetTrianglePoint(ref tempRightTriangle, rightCount, triangle, i);
                rightCount++;
            }
        }

        // find the new triangles X 3
        // first the new vertices

        // this will give me a triangle with the solo point as first
        if (leftCount == 1)
        {
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 0, tempLeftTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 1, tempRightTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 2, tempRightTriangle, 1);
        }
        else // rightCount == 1
        {
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 0, tempRightTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 1, tempLeftTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 2, tempLeftTriangle, 1);
        }

        // now to find the intersection points between the solo point and the others
        float distance = 0;
        float normalizedDistance = 0.0f;
        Vector3 edgeVector = Vector3.zero; // contains edge length and direction

        edgeVector = tempTriangle.vertices[1] - tempTriangle.vertices[0];
        blade.Raycast(new Ray(tempTriangle.vertices[0], edgeVector.normalized), out distance);

        normalizedDistance = distance / edgeVector.magnitude;
        newTriangle.vertices[0] = Vector3.Lerp(tempTriangle.vertices[0], tempTriangle.vertices[1], normalizedDistance);
        newTriangle.uvs[0] = Vector2.Lerp(tempTriangle.uvs[0], tempTriangle.uvs[1], normalizedDistance);
        newTriangle.normals[0] = Vector3.Lerp(tempTriangle.normals[0], tempTriangle.normals[1], normalizedDistance);
        newTriangle.tangents[0] = Vector4.Lerp(tempTriangle.tangents[0], tempTriangle.tangents[1], normalizedDistance);

        edgeVector = tempTriangle.vertices[2] - tempTriangle.vertices[0];
        blade.Raycast(new Ray(tempTriangle.vertices[0], edgeVector.normalized), out distance);

        normalizedDistance = distance / edgeVector.magnitude;
        newTriangle.vertices[1] = Vector3.Lerp(tempTriangle.vertices[0], tempTriangle.vertices[2], normalizedDistance);
        newTriangle.uvs[1] = Vector2.Lerp(tempTriangle.uvs[0], tempTriangle.uvs[2], normalizedDistance);
        newTriangle.normals[1] = Vector3.Lerp(tempTriangle.normals[0], tempTriangle.normals[2], normalizedDistance);
        newTriangle.tangents[1] = Vector4.Lerp(tempTriangle.tangents[0], tempTriangle.tangents[2], normalizedDistance);

        if (newTriangle.vertices[0] != newTriangle.vertices[1])
        {
            //tracking newly created points
            addedVertices.Add(newTriangle.vertices[0]);
            addedVertices.Add(newTriangle.vertices[1]);
        }
        // make the new triangles
        // one side will get 1 the other will get 2

        if (leftCount == 1)
        {
            // first one on the left
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 0, tempLeftTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 1, newTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 2, newTriangle, 1);

            // check if it is facing the right way
            NormalCheck(ref tempTriangle);

            // add it
            leftSide.AddTriangle(tempTriangle, submesh);


            // other two on the right
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 0, tempRightTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 1, newTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 2, newTriangle, 1);

            // check if it is facing the right way
            NormalCheck(ref tempTriangle);

            // add it
            rightSide.AddTriangle(tempTriangle, submesh);

            // third
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 0, tempRightTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 1, tempRightTriangle, 1);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 2, newTriangle, 1);

            // check if it is facing the right way
            NormalCheck(ref tempTriangle);

            // add it
            rightSide.AddTriangle(tempTriangle, submesh);
        }
        else
        {
            // first one on the right
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 0, tempRightTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 1, newTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 2, newTriangle, 1);

            // check if it is facing the right way
            NormalCheck(ref tempTriangle);

            // add it
            rightSide.AddTriangle(tempTriangle, submesh);


            // other two on the left
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 0, tempLeftTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 1, newTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 2, newTriangle, 1);

            // check if it is facing the right way
            NormalCheck(ref tempTriangle);

            // add it
            leftSide.AddTriangle(tempTriangle, submesh);

            // third
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 0, tempLeftTriangle, 0);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 1, tempLeftTriangle, 1);
            AdvancedMesh.SetTrianglePoint(ref tempTriangle, 2, newTriangle, 1);

            // check if it is facing the right way
            NormalCheck(ref tempTriangle);

            // add it
            leftSide.AddTriangle(tempTriangle, submesh);
        }

    }

    static void CapTheCut()
    {

        usedIndeces.Clear();
        polygonIndeces.Clear();

        // find the needed polygons
        // the cut faces added new vertices by 2 each time to make an edge
        // if two edges contain the same Vector3 point, they are connected
        for (int i = 0; i < addedVertices.Count; i += 2)
        {
            // check the edge
            if (!usedIndeces.Contains(i)) // if it has one, it has this edge
            {
                //new polygon started with this edge
                polygonIndeces.Clear();
                polygonIndeces.Add(i);
                polygonIndeces.Add(i + 1);

                usedIndeces.Add(i);
                usedIndeces.Add(i + 1);

                Vector3 connectionPointLeft = addedVertices[i];
                Vector3 connectionPointRight = addedVertices[i + 1];
                bool isDone = false;

                // look for more edges
                while (!isDone)
                {
                    isDone = true;

                    // loop through edges
                    for (int index = 0; index < addedVertices.Count; index += 2)
                    {   // if it has one, it has this edge
                        if (!usedIndeces.Contains(index))
                        {
                            Vector3 nextPoint1 = addedVertices[index];
                            Vector3 nextPoint2 = addedVertices[index + 1];

                            // check for next point in the chain
                            if (connectionPointLeft == nextPoint1 || connectionPointLeft == nextPoint2 ||
                                connectionPointRight == nextPoint1 || connectionPointRight == nextPoint2)
                            {
                                usedIndeces.Add(index);
                                usedIndeces.Add(index + 1);

                                // add the other
                                if (connectionPointLeft == nextPoint1)
                                {
                                    polygonIndeces.Insert(0, index + 1);
                                    connectionPointLeft = addedVertices[index + 1];
                                }
                                else if (connectionPointLeft == nextPoint2)
                                {
                                    polygonIndeces.Insert(0, index);
                                    connectionPointLeft = addedVertices[index];
                                }
                                else if (connectionPointRight == nextPoint1)
                                {
                                    polygonIndeces.Add(index + 1);
                                    connectionPointRight = addedVertices[index + 1];
                                }
                                else if (connectionPointRight == nextPoint2)
                                {
                                    polygonIndeces.Add(index);
                                    connectionPointRight = addedVertices[index];
                                }

                                isDone = false;
                            }
                        }
                    }
                }// while isDone = False

                // check if the link is closed
                // first == last
                if (addedVertices[polygonIndeces[0]] == addedVertices[polygonIndeces[polygonIndeces.Count - 1]])
                    polygonIndeces[polygonIndeces.Count - 1] = polygonIndeces[0];
                else
                    polygonIndeces.Add(polygonIndeces[0]);

                // cap
                FillCap(polygonIndeces);
            }
        }
    }
    static void FillCap(List<int> indices)
    {

        // center of the cap
        Vector3 center = Vector3.zero;
        foreach (var index in indices)
            center += addedVertices[index];

        center = center / indices.Count;

        // you need an axis based on the cap
        Vector3 upward = Vector3.zero;
        // 90 degree turn
        upward.x = blade.normal.y;
        upward.y = -blade.normal.x;
        upward.z = blade.normal.z;
        Vector3 left = Vector3.Cross(blade.normal, upward);

        Vector3 displacement = Vector3.zero;
        Vector2 newUV1 = Vector2.zero;
        Vector2 newUV2 = Vector2.zero;
        Vector2 newUV3 = Vector2.zero;

        // indices should be in order like a closed chain

        // go through edges and eliminate by creating triangles with connected edges
        // each new triangle removes 2 edges but creates 1 new edge
        // keep the chain in order
        int iterator = 0;
        while (indices.Count > 2)
        {

            Vector3 link1 = addedVertices[indices[iterator]];
            Vector3 link2 = addedVertices[indices[(iterator + 1) % indices.Count]];
            Vector3 link3 = addedVertices[indices[(iterator + 2) % indices.Count]];

            displacement = link1 - center;
            newUV1 = Vector3.zero;
            newUV1.x = 0.5f + Vector3.Dot(displacement, left);
            newUV1.y = 0.5f + Vector3.Dot(displacement, upward);

            displacement = link2 - center;
            newUV2 = Vector3.zero;
            newUV2.x = 0.5f + Vector3.Dot(displacement, left);
            newUV2.y = 0.5f + Vector3.Dot(displacement, upward);

            displacement = link3 - center;
            newUV3 = Vector3.zero;
            newUV3.x = 0.5f + Vector3.Dot(displacement, left);
            newUV3.y = 0.5f + Vector3.Dot(displacement, upward);


            // add triangle
            AdvancedMesh.SetTrianglePoint(ref newTriangle, 0, link1, newUV1, -blade.normal, Vector4.zero);
            AdvancedMesh.SetTrianglePoint(ref newTriangle, 1, link2, newUV2, -blade.normal, Vector4.zero);
            AdvancedMesh.SetTrianglePoint(ref newTriangle, 2, link3, newUV3, -blade.normal, Vector4.zero);

            // add to left side
            NormalCheck(ref newTriangle);

            leftSide.AddTriangle(newTriangle, cutMatSubmesh);

            // add to right side
            newTriangle.normals[0] = blade.normal;
            newTriangle.normals[1] = blade.normal;
            newTriangle.normals[2] = blade.normal;

            NormalCheck(ref newTriangle);

            rightSide.AddTriangle(newTriangle, cutMatSubmesh);


            // adjust indices by removing the middle link
            indices.RemoveAt((iterator + 1) % indices.Count);

            // move on
            iterator = (iterator + 1) % indices.Count;
        }

    }

    static void NormalCheck(ref AdvancedMesh.Triangle triangle)
    {
        Vector3 crossProduct = Vector3.Cross(triangle.vertices[1] - triangle.vertices[0], triangle.vertices[2] - triangle.vertices[0]);
        Vector3 averageNormal = (triangle.normals[0] + triangle.normals[1] + triangle.normals[2]) / 3.0f;
        float dotProduct = Vector3.Dot(averageNormal, crossProduct);
        if (dotProduct < 0)
        {
            SwapTrianglePoints(ref triangle, 0, 2);
        }

    }

    static void Swap<T>(ref T item1, ref T item2)
    {
        T temp = item1;
        item1 = item2;
        item2 = temp;
    }

    static void SwapTrianglePoints(ref AdvancedMesh.Triangle triangle, int point1, int point2)
    {
            Swap(ref triangle.vertices[point1], ref triangle.vertices[point2]);
            Swap(ref triangle.normals[point1], ref triangle.normals[point2]);
            Swap(ref triangle.uvs[point1], ref triangle.uvs[point2]);
            Swap(ref triangle.tangents[point1], ref triangle.tangents[point2]);
    }
}


public class AdvancedMesh
{
    public List<Vector3> _vertices { get; private set; } = new List<Vector3>();
    public List<Vector3> _normals { get; private set; } = new List<Vector3>();
    public List<Vector2> _uvs { get; private set; } = new List<Vector2>();
    public List<Vector4> _tangents { get; private set; } = new List<Vector4>();
    public List<List<int>> _subIndices { get; private set; } = new List<List<int>>();

    public int VertCount
    {
        get
        {
            return _vertices.Count;
        }
    }

    public void Clear()
    {
        _vertices.Clear();
        _normals.Clear();
        _uvs.Clear();
        _tangents.Clear();
        _subIndices.Clear();
    }

    public void AddTriangle(Triangle triangle, int submesh)
    {
        AddTriangle(triangle.vertices, triangle.uvs, triangle.normals, triangle.tangents, submesh);
    }

    public void AddTriangle(Vector3[] vertices, Vector2[] uvs, Vector3[] normals, int submesh = 0)
    {
        AddTriangle(vertices, uvs, normals, null, submesh);
    }

    public void AddTriangle(Vector3[] vertices, Vector2[] uvs, Vector3[] normals, Vector4[] tangents, int submesh = 0)
    {
        int vertCount = _vertices.Count;

        _vertices.Add(vertices[0]);
        _vertices.Add(vertices[1]);
        _vertices.Add(vertices[2]);

        _normals.Add(normals[0]);
        _normals.Add(normals[1]);
        _normals.Add(normals[2]);

        _uvs.Add(uvs[0]);
        _uvs.Add(uvs[1]);
        _uvs.Add(uvs[2]);

        if (tangents != null)
        {
            _tangents.Add(tangents[0]);
            _tangents.Add(tangents[1]);
            _tangents.Add(tangents[2]);
        }

        if (_subIndices.Count < submesh + 1)
        {
            for (int i = _subIndices.Count; i < submesh + 1; i++)
            {
                _subIndices.Add(new List<int>());
            }
        }

        _subIndices[submesh].Add(vertCount);
        _subIndices[submesh].Add(vertCount + 1);
        _subIndices[submesh].Add(vertCount + 2);

    }

    public void RemoveDoubles()
    {

        int dubCount = 0;

        Vector3 vertex = Vector3.zero;
        Vector3 normal = Vector3.zero;
        Vector2 uv = Vector2.zero;
        Vector4 tangent = Vector4.zero;

        int iterator = 0;
        while (iterator < VertCount)
        {

            vertex = _vertices[iterator];
            normal = _normals[iterator];
            uv = _uvs[iterator];

            // look backwards for a match
            for (int backward_iterator = iterator - 1; backward_iterator >= 0; backward_iterator--)
            {

                if (vertex == _vertices[backward_iterator] &&
                    normal == _normals[backward_iterator] &&
                    uv == _uvs[backward_iterator])
                {
                    dubCount++;
                    DoubleFound(backward_iterator, iterator);
                    iterator--;
                    break; // there should only be one
                }
            }

            iterator++;

        } // while

        Debug.LogFormat("Doubles found {0}", dubCount);

    }

    private void DoubleFound(int first, int duplicate)
    {
        for (int h = 0; h < _subIndices.Count; h++)
        {
            for (int i = 0; i < _subIndices[h].Count; i++)
            {

                if (_subIndices[h][i] > duplicate) // knock it down
                    _subIndices[h][i]--;
                else if (_subIndices[h][i] == duplicate) // replace
                    _subIndices[h][i] = first;
            }
        }

        _vertices.RemoveAt(duplicate);
        _normals.RemoveAt(duplicate);
        _uvs.RemoveAt(duplicate);

        if (_tangents.Count > 0)
            _tangents.RemoveAt(duplicate);

    }

    public Mesh GetMesh()
    {

        Mesh shape = new Mesh();
        shape.name = "Generated Mesh";
        shape.SetVertices(_vertices);
        shape.SetNormals(_normals);
        shape.SetUVs(0, _uvs);
        shape.SetUVs(1, _uvs);

        if (_tangents.Count > 1)
            shape.SetTangents(_tangents);

        shape.subMeshCount = _subIndices.Count;

        for (int i = 0; i < _subIndices.Count; i++)
            shape.SetTriangles(_subIndices[i], i);

        return shape;
    }

    public struct Triangle
    {
        public Vector3[] vertices;
        public Vector2[] uvs;
        public Vector3[] normals;
        public Vector4[] tangents;

        public Triangle(Vector3[] vertices = null, Vector2[] uvs = null, Vector3[] normals = null, Vector4[] tangents = null)
        {
            this.vertices = vertices;
            this.uvs = uvs;
            this.normals = normals;
            this.tangents = tangents;
        }
    }

    public static Triangle NewTriangle()
    {
        return new Triangle(new Vector3[3], new Vector2[3], new Vector3[3], new Vector4[3]);
    }

    public static void SetTriangle(ref Triangle toSet, Mesh mesh, int ind1, int ind2, int ind3)
    {
        // verts
        toSet.vertices[0] = mesh.vertices[ind1];
        toSet.vertices[1] = mesh.vertices[ind2];
        toSet.vertices[2] = mesh.vertices[ind3];

        // normals
        toSet.normals[0] = mesh.normals[ind1];
        toSet.normals[1] = mesh.normals[ind2];
        toSet.normals[2] = mesh.normals[ind3];

        // uvs
        toSet.uvs[0] = mesh.uv[ind1];
        toSet.uvs[1] = mesh.uv[ind2];
        toSet.uvs[2] = mesh.uv[ind3];

        // tangents
        if (mesh.tangents != null && mesh.tangents.Length != 0)
        {
            toSet.tangents[0] = mesh.tangents[ind1];
            toSet.tangents[1] = mesh.tangents[ind2];
            toSet.tangents[2] = mesh.tangents[ind3];
        }
        else
        {
            toSet.tangents[0] = Vector4.zero;
            toSet.tangents[1] = Vector4.zero;
            toSet.tangents[2] = Vector4.zero;
        }
    }

    public static void SetTrianglePoint(ref Triangle toSet, int toPointIndex, Triangle fromSet, int fromPointIndex)
    {
        toSet.vertices[toPointIndex] = fromSet.vertices[fromPointIndex];
        toSet.uvs[toPointIndex] = fromSet.uvs[fromPointIndex];
        toSet.normals[toPointIndex] = fromSet.normals[fromPointIndex];
        toSet.tangents[toPointIndex] = fromSet.tangents[fromPointIndex];
    }

    public static void SetTrianglePoint(ref Triangle toSet, int toPointIndex, Vector3 vertex, Vector2 uv, Vector3 normal, Vector4 tangent)
    {
        toSet.vertices[toPointIndex] = vertex;
        toSet.uvs[toPointIndex] = uv;
        toSet.normals[toPointIndex] = normal;
        toSet.tangents[toPointIndex] = tangent;
    }
}