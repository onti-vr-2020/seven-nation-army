﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class CutMeshScript : MonoBehaviour
{
    static Plane blade;
    static Mesh targetMesh, LMesh, RMesh;
    static bool[] pointSign = new bool[3];
    public static GameObject[] Cut(GameObject target, Vector3 bladePos, Vector3 normal)
    {
        List<int> lTriangles = new List<int>(), rTriangles = new List<int>();
        List<Vector3> lVertices = new List<Vector3>(), rVertices = new List<Vector3>();

        normal = target.transform.InverseTransformDirection(-normal);
        blade = new Plane(normal, target.transform.InverseTransformPoint(bladePos));
        targetMesh = target.GetComponent<MeshFilter>().mesh;

        int p1, p2, p3;
        for(int i = 0; i < targetMesh.triangles.Length; i++)
        {
            List<int> lPoints = new List<int>(), rPoints = new List<int>();
            p1 = targetMesh.triangles[i];
            pointSign[0] = Vector3.Dot(normal, targetMesh.vertices[p1]) >= 0;
            if (pointSign[0]) rPoints.Add(p1);
            else lPoints.Add(p1);
            p2 = targetMesh.triangles[i + 1];
            pointSign[1] = Vector3.Dot(normal, targetMesh.vertices[p2]) >= 0;
            if (pointSign[1]) rPoints.Add(p2);
            else lPoints.Add(p2);
            p3 = targetMesh.triangles[i + 2];
            pointSign[2] = Vector3.Dot(normal, targetMesh.vertices[p3]) >= 0;
            if (pointSign[2]) rPoints.Add(p3);
            else lPoints.Add(p3);

            if (lPoints.Count == 0)
            {
                //rVertices.Add(targetMesh.vertices[)
            }
        }

        return new GameObject[]{ null };
    }

    //class Vector2
    //{
    //    public decimal x, y;
    //    public Vector2(decimal a = 0, decimal b = 0)
    //    {
    //        x = a; y = b;
    //    }
    //};
    //public class Vector3 : Program, IComparable
    //{
    //    public decimal x, y, z;
    //    public Vector3(decimal x = 0, decimal y = 0, decimal z = 0)
    //    {
    //        this.x = x;
    //        this.y = y;
    //        this.z = z;
    //    }
    //    public Vector3()
    //    {
    //        x = y = z = 0;
    //    }
    //    public static Vector3 operator -(Vector3 a, Vector3 b)
    //    {
    //        return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
    //    }
    //    public static Vector3 operator +(Vector3 a, Vector3 b)
    //    {
    //        return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
    //    }
    //    public static Vector3 operator *(Vector3 a, decimal b)
    //    {
    //        return new Vector3(a.x * b, a.y * b, a.z * b);
    //    }
    //    public static Vector3 operator /(Vector3 a, decimal b)
    //    {
    //        return new Vector3(a.x / b, a.y / b, a.z / b);
    //    }

    //    public static decimal Hypot(decimal x, decimal y)
    //    {
    //        return sqrt(x * x + y * y);
    //    }

    //    public int CompareTo(object o)
    //    {
    //        Vector3 b = o as Vector3;
    //        if (x < b.x) return -1;
    //        if (x == b.x && y < b.y) return -1;
    //        if (x == b.x && y == b.y && z < b.z) return -1;
    //        if (x == b.x && y == b.y && z == b.z) return 0;
    //        return 1;
    //    }
    //}

    //public class pair<T1, T2>
    //{
    //    public T1 first;
    //    public T2 second;
    //    public pair(T1 a, T2 b)
    //    {
    //        first = a; second = b;
    //    }
    //}
    //public class Polar : Program, IComparer<pair<Vector3, int>>
    //{
    //    Vector3 I, J, p0, t1;
    //    public Polar(Vector3 I, Vector3 J, Vector3 p0, Vector3 t1)
    //    {
    //        this.I = I; this.J = J; this.p0 = p0; this.t1 = t1;
    //        normalize(ref I); normalize(ref J);
    //    }
    //    public static decimal Hypot(decimal x, decimal y)
    //    {
    //        return sqrt(x * x + y * y);
    //    }
    //    public int Compare(pair<Vector3, int> al, pair<Vector3, int> be)
    //    {
    //        Vector3 A = al.first - t1, B = be.first - t1;
    //        Vector2 a = new Vector2(dot(I, A), dot(J, A)),
    //            b = new Vector2(dot(I, B), dot(J, B)),
    //            st = new Vector2(dot(I, p0 - t1), dot(J, p0 - t1));
    //        if ((a.x - b.x) * (b.y - st.y) - (b.x - st.x) * (a.y - b.y) > 0)
    //        {
    //            return -1;
    //        }
    //        else if ((a.x - b.x) * (b.y - st.y) - (b.x - st.x) * (a.y - b.y) == 0)
    //        {
    //            if (Hypot(a.x - st.x, a.y - st.y) > Hypot(b.x - st.x, b.y - st.y)) return -1;
    //        }
    //        return 1;
    //    }
    //}
    //public class Program
    //{
    //    #region functions
    //    public static decimal sqrt(decimal x)
    //    {
    //        double sq = Math.Sqrt(double.Parse(x.ToString()));
    //        decimal eps = decimal.Parse("0.00000000000000001"), l = (decimal)sq, r = (decimal)sq + 1, m = (l + r) / 2;
    //        for (int i = 0; i < 100; i++)
    //        {
    //            m = (r + l) / 2;
    //            if (m * m < x) l = m;
    //            else r = m + eps;
    //        }
    //        return l;
    //    }
    //    public static Vector3[,] t = new Vector3[2, 4];
    //    public static pair<Vector3, Vector3>[,] border = new pair<Vector3, Vector3>[2, 6];
    //    public static Tuple<Vector3, Vector3, Vector3>[,] plane = new Tuple<Vector3, Vector3, Vector3>[2, 4];
    //    public static decimal dot(Vector3 a, Vector3 b)
    //    {
    //        return a.x * b.x + a.y * b.y + a.z * b.z;
    //    }
    //    public static Vector3 cross(Vector3 a, Vector3 b)
    //    {
    //        return new Vector3(a.y * b.z - b.y * a.z,
    //            a.z * b.x - b.z * a.x,
    //            a.x * b.y - b.x * a.y);
    //    }
    //    public static decimal mixcross(Vector3 a, Vector3 b, Vector3 c)
    //    {
    //        return dot(a, cross(b, c));
    //    }
    //    static decimal len(Vector3 a)
    //    {
    //        decimal d = a.x * a.x + a.y * a.y + a.z * a.z;
    //        return sqrt(d);
    //    }
    //    public static void normalize(ref Vector3 a)
    //    {
    //        decimal l = len(a);
    //        if (l == 0) return;
    //        a = new Vector3(a.x / l, a.y / l, a.z / l);
    //    }
    //    static decimal angle(Vector3 a, Vector3 b, Vector3 N)
    //    {
    //        normalize(ref N);
    //        decimal y = dot(cross(a, b), N), x = dot(a, b);
    //        return (decimal)Math.Atan2(double.Parse(y.ToString()), double.Parse(x.ToString()));//[-PI;PI]
    //    }
    //    static decimal sinSigned(Vector3 a, Vector3 b, Vector3 N)
    //    {
    //        normalize(ref N); normalize(ref a); normalize(ref b);
    //        if (len(a) == 0 || len(b) == 0) return 0;
    //        return dot(cross(a, b), N);
    //    }
    //    static decimal sSigned(Vector3 a, Vector3 b, Vector3 N)
    //    {
    //        decimal sin = sinSigned(a, b, N);
    //        Vector3 c = cross(a, b);
    //        decimal l = len(c);
    //        if (sin > 0) return l;
    //        return -1 * l;
    //    }
    //    static void InitPlane(ref Vector3 I, ref Vector3 J, ref Vector3 K)
    //    {
    //        //i - I vector of the plane, b - some other vector of the plane
    //        //pts - points on the plane to polar sort
    //        K = cross(I, J);
    //        J = cross(K, I);
    //        normalize(ref I); normalize(ref J); normalize(ref K);
    //        //I=x, J = y; K = z of the plane
    //    }
    //    static Vector3 InitPolarForPlane(Vector3 t1, Vector3 I, Vector3 J, ref List<pair<Vector3, int>> pts)
    //    {
    //        normalize(ref I); normalize(ref J);

    //        Vector3 p0 = pts[0].first;
    //        decimal x1 = dot(I, p0 - t1), y1 = dot(J, p0 - t1);
    //        for (int t = 1; t < pts.Count; t++)
    //        {
    //            decimal x2 = dot(I, pts[t].first - t1), y2 = dot(J, pts[t].first - t1);
    //            if (y2 < y1 || (y2 == y1 && x2 < x1))
    //            {
    //                p0 = pts[t].first;
    //                x1 = x2; y1 = y2;
    //            }
    //        }
    //        return p0;
    //    }
    //    static bool inTriangle(Vector3 t1, Vector3 t2, Vector3 t3, Vector3 a)
    //    {
    //        Vector3 N = cross(t2 - t1, t3 - t1);
    //        return (angle(t2 - t1, a - t1, N) * angle(t2 - t1, t3 - t1, N) >= 0) &&
    //            (angle(t3 - t1, a - t1, N) * angle(t3 - t1, t2 - t1, N) >= 0) &&
    //            (angle(t3 - t2, a - t2, N) * angle(t3 - t2, t1 - t2, N) >= 0);
    //    }


    //    static bool InTetraedr(int k, Vector3 a)
    //    {
    //        bool flag = true;
    //        for (int i = 0; i < 4; i++)
    //        {
    //            Vector3 I = plane[k, i].Item2 - plane[k, i].Item1, J = plane[k, i].Item3 - plane[k, i].Item1, K = new Vector3();
    //            InitPlane(ref I, ref J, ref K);
    //            decimal x = dot(I, a - plane[k, i].Item1), y = dot(J, a - plane[k, i].Item1);
    //            Vector3 proj = plane[k, i].Item1 + I * x + J * y;
    //            flag = flag && inTriangle(plane[k, i].Item1, plane[k, i].Item2, plane[k, i].Item3, proj);
    //        }
    //        return flag;
    //    }

    //    public static bool Equal(Vector3 a, Vector3 b)
    //    {
    //        decimal eps = decimal.Parse("0.000000001");
    //        return !(Math.Abs(a.x - b.x) > eps || Math.Abs(a.y - b.y) > eps || Math.Abs(a.z - b.z) > eps);
    //    }

    //    public static void Swap<T>(ref T a, ref T b)
    //    {
    //        T x = a;
    //        a = b;
    //        b = x;
    //    }
    //    #endregion

    //    static void Main(string[] args)
    //    {
    //        for (int i = 0; i < 4; i++)
    //        {
    //            List<string> tmp = Console.ReadLine().Trim().Split(' ').ToList();
    //            tmp.RemoveAll(s => string.IsNullOrEmpty(s));
    //            List<decimal> lst = tmp.Select(x => decimal.Parse(x)).ToList();
    //            t[0, i] = new Vector3(lst[0], lst[1], lst[2]);
    //        }
    //        Console.ReadLine();
    //        for (int i = 0; i < 4; i++)
    //        {
    //            List<string> tmp = Console.ReadLine().Trim().Split(' ').ToList();
    //            tmp.RemoveAll(s => string.IsNullOrEmpty(s));
    //            List<decimal> lst = tmp.Select(x => decimal.Parse(x)).ToList();
    //            t[1, i] = new Vector3(lst[0], lst[1], lst[2]);
    //        }
    //        //if (t[0, 0].x == 0) t[0, 0] = t[2, 0];


    //        for (int i = 0; i < 2; i++)
    //        {
    //            border[i, 0] = new pair<Vector3, Vector3>(t[i, 0], t[i, 1]);
    //            border[i, 1] = new pair<Vector3, Vector3>(t[i, 0], t[i, 2]);
    //            border[i, 2] = new pair<Vector3, Vector3>(t[i, 0], t[i, 3]);
    //            border[i, 3] = new pair<Vector3, Vector3>(t[i, 1], t[i, 2]);
    //            border[i, 4] = new pair<Vector3, Vector3>(t[i, 2], t[i, 3]);
    //            border[i, 5] = new pair<Vector3, Vector3>(t[i, 3], t[i, 1]);
    //            plane[i, 0] = new Tuple<Vector3, Vector3, Vector3>(t[i, 0], t[i, 1], t[i, 2]);
    //            plane[i, 1] = new Tuple<Vector3, Vector3, Vector3>(t[i, 0], t[i, 2], t[i, 3]);
    //            plane[i, 2] = new Tuple<Vector3, Vector3, Vector3>(t[i, 0], t[i, 3], t[i, 1]);
    //            plane[i, 3] = new Tuple<Vector3, Vector3, Vector3>(t[i, 1], t[i, 2], t[i, 3]);
    //        }

    //        List<Vector3> pts = new List<Vector3>();
    //        SortedSet<Vector3> spts = new SortedSet<Vector3>();
    //        for (int k = 0; k < 2; k++)//tertaedr
    //        {
    //            int rev = (k == 0 ? 1 : 0);
    //            for (int j = 0; j < 4; j++)//plane
    //            {
    //                for (int i = 0; i < 6; i++)//border
    //                {
    //                    //                       t2           -        t1          ,        t3           -          t1
    //                    Vector3 N = cross(plane[rev, j].Item2 - plane[rev, j].Item1, plane[rev, j].Item3 - plane[rev, j].Item1);
    //                    normalize(ref N);
    //                    Vector3 V = plane[rev, j].Item1 - border[k, i].first;
    //                    decimal d = dot(N, V);
    //                    Vector3 W = border[k, i].second - border[k, i].first;
    //                    decimal e = dot(N, W);
    //                    if (e != 0 && Math.Abs(d / e) > 1000000) e = 0;
    //                    if (e != 0)
    //                    {
    //                        Vector3 O = border[k, i].first + W * d / e;
    //                        if (inTriangle(plane[rev, j].Item1, plane[rev, j].Item2, plane[rev, j].Item3, O) && dot(border[k, i].first - O, border[k, i].second - O) <= 0) spts.Add(O);
    //                    }
    //                }
    //            }
    //        }
    //        decimal v = 0;
    //        for (int i = 0; i < 2; i++)
    //        {
    //            int rev = i == 0 ? 1 : 0;
    //            for (int j = 0; j < 4; j++)
    //            {
    //                if (InTetraedr(rev, t[i, j])) spts.Add(t[i, j]);
    //            }
    //        }
    //        foreach (Vector3 i in spts)
    //        {
    //            if (pts.Count == 0) pts.Add(i);
    //            else
    //            {
    //                bool flag = true;
    //                for (int j = 0; j < pts.Count; j++) if (Equal(pts[j], i)) flag = false;
    //                if (flag) pts.Add(i);
    //            }
    //        }
    //        Vector3 center = new Vector3();
    //        foreach (Vector3 i in pts) center += i / pts.Count;
    //        Dictionary<Tuple<int, int, int>, bool> was = new Dictionary<Tuple<int, int, int>, bool>();
    //        SortedSet<decimal> VV = new SortedSet<decimal>();
    //        for (int i = 0; i < pts.Count; i++)
    //        {
    //            for (int j = 0; j < pts.Count; j++)
    //            {
    //                if (i == j) continue;
    //                for (int k = 0; k < pts.Count; k++)
    //                {
    //                    if (k == i || k == j) continue;
    //                    Vector3 I = pts[j] - pts[i], J = pts[k] - pts[i], N = cross(I, J);
    //                    //if (dot(N, center - pts[i]) > 0) continue;
    //                    bool pos = true, flag = false;
    //                    List<pair<Vector3, int>> pln = new List<pair<Vector3, int>>();
    //                    pln.Add(new pair<Vector3, int>(pts[i], i)); pln.Add(new pair<Vector3, int>(pts[j], j)); pln.Add(new pair<Vector3, int>(pts[k], k));
    //                    for (int m = 0; m < pts.Count; m++)
    //                    {
    //                        if (m != i && m != j && m != k)
    //                        {
    //                            decimal eps = decimal.Parse("0.00001");
    //                            if (Math.Abs(dot(N, pts[m] - pts[i])) < eps)
    //                            {
    //                                pln.Add(new pair<Vector3, int>(pts[m], m));
    //                                continue;
    //                            }
    //                            if (!flag)
    //                            {
    //                                flag = true;
    //                                pos = dot(N, pts[m] - pts[i]) > eps;
    //                            }
    //                            else if (Math.Abs(dot(N, pts[m] - pts[i])) < 2 * eps) ;
    //                            else if (pos != (dot(N, pts[m] - pts[i]) > eps)) goto A;
    //                        }
    //                    }
    //                    Vector3 t1 = pln[0].first;
    //                    if (pln.Count > 3)
    //                    {
    //                        I = pln[1].first - t1; J = pln[2].first - t1;
    //                        Vector3 K = cross(I, J);
    //                        J = cross(K, I);
    //                        Vector3 p0 = InitPolarForPlane(t1, I, J, ref pln);
    //                        pln.Sort(new Polar(I, J, p0, t1));
    //                    }
    //                    for (int tr = 1; tr < pln.Count - 1; tr++)
    //                    {
    //                        int[] coord = new int[3];
    //                        coord[0] = pln[0].second; coord[1] = pln[tr].second; coord[2] = pln[tr + 1].second;
    //                        Array.Sort(coord);
    //                        if (was.ContainsKey(new Tuple<int, int, int>(coord[0], coord[1], coord[2]))) continue;
    //                        was.Add(new Tuple<int, int, int>(coord[0], coord[1], coord[2]), true);
    //                        v += Math.Abs(mixcross(pln[0].first - center, pln[tr].first - center, pln[tr + 1].first - center) / 6);
    //                        VV.Add(Math.Abs(mixcross(pln[0].first - pts[0], pln[tr].first - pts[0], pln[tr + 1].first - pts[0]) / 6));
    //                    }

    //                    for (int _i = 0; _i < pln.Count; _i++)
    //                    {
    //                        for (int _j = 0; _j < pln.Count; _j++)
    //                        {
    //                            for (int _k = 0; _k < pln.Count; _k++)
    //                            {
    //                                if (_i == _j || _i == _k || _j == _k) continue;
    //                                int[] _tmp = new int[3];
    //                                _tmp[0] = pln[_i].second; _tmp[1] = pln[_j].second; _tmp[2] = pln[_k].second;
    //                                Array.Sort(_tmp);
    //                                if (!was.ContainsKey(new Tuple<int, int, int>(_tmp[0], _tmp[1], _tmp[2]))) was.Add(new Tuple<int, int, int>(_tmp[0], _tmp[1], _tmp[2]), false);
    //                            }
    //                        }
    //                    }
    //                A:;
    //                }
    //            }
    //        }

    //        v = Math.Abs(v);
    //        Console.WriteLine(string.Format("{0:0.0000000}", v));
    //        Console.ReadLine();
    //    }

    //}
}
