﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class CheckerScript : MonoBehaviour
{
    CheckScript par;
    void Start()
    {
        par = GetComponentInParent<CheckScript>();
    }

    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (par.inTemplate > 0 && par.inWork > 0) par.both++;
    }


    private void OnTriggerEnter(Collider collision)
    {
        string tag = collision.gameObject.tag;
        if (tag == par.templateTag) par.inTemplate++;
        else if (tag == par.pieceTag) par.inWork++;
    }
    private void OnTriggerExit(Collider collision)
    {
        string tag = collision.gameObject.tag;
        if (tag == par.templateTag) par.inTemplate--;
        else if (tag == par.pieceTag) par.inWork--;
    }
}

