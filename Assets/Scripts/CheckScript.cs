﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckScript : MonoBehaviour
{
    Mesh template, work;
    List<GameObject> pieces;
    public string templateTag;
    public string pieceTag;
    [SerializeField] GameObject workObject;
    public BoxCollider bounds;
    [SerializeField] GameObject helper;
    [SerializeField] Color lineColor;
    [SerializeField] float lineWidth;
    [HideInInspector] public int inTemplate = 0, inWork = 0, both = 0, all = 0;
    public float score { get; private set; }
    List<Transform> helpers = new List<Transform>();
    void Start()
    {
        template = GameObject.FindGameObjectWithTag(templateTag).GetComponent<MeshFilter>().sharedMesh;
        int linesCount = 0;
        for (float x = bounds.bounds.min.x; x <= bounds.bounds.max.x; x += helper.transform.localScale.x)
        {
            for (float y = bounds.bounds.min.y; y <= bounds.bounds.max.y; y += helper.transform.localScale.y)
            {
                GameObject g = Instantiate(helper, new Vector3(x, y, bounds.bounds.min.z), Quaternion.identity, transform);
                g.SetActive(true);
                helpers.Add(g.transform);

            }
        }

        /*for (float x = bounds.bounds.min.x; x <= bounds.bounds.max.x; x += helper.transform.localScale.x)
        {
            for (float y = bounds.bounds.min.y; y <= bounds.bounds.max.y; y += helper.transform.localScale.y)
            {
                GameObject line = new GameObject("Line" + linesCount++, typeof(LineRenderer));
                line.transform.parent = transform;
                LineRenderer lineRenderer = line.GetComponent<LineRenderer>();
                lineRenderer.useWorldSpace = true;
                lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                lineRenderer.material = new Material(Shader.Find("Legacy Shaders/Particles/Additive"));
                // update line renderer
                lineRenderer.startColor = lineColor;
                lineRenderer.endColor = lineColor;
                lineRenderer.startWidth = lineWidth;
                lineRenderer.endWidth = lineWidth;
                lineRenderer.positionCount = 2;
                Vector3 p0 = new Vector3(x, y, bounds.bounds.min.z);
                Vector3 p1 = new Vector3(x, y, bounds.bounds.max.z);
                lineRenderer.SetPosition(0, p0);
                lineRenderer.SetPosition(1, p1);
            }
        }
        for (float y = bounds.bounds.min.y; y <= bounds.bounds.max.y; y += helper.transform.localScale.y)
        {
            for (float z = bounds.bounds.min.z; z <= bounds.bounds.max.z; z += helper.transform.localScale.z)
            {
                GameObject line = new GameObject("Line" + linesCount++, typeof(LineRenderer));
                line.transform.parent = transform;
                LineRenderer lineRenderer = line.GetComponent<LineRenderer>();
                lineRenderer.useWorldSpace = true;
                lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                lineRenderer.material = new Material(Shader.Find("Legacy Shaders/Particles/Additive"));
                // update line renderer
                lineRenderer.startColor = lineColor;
                lineRenderer.endColor = lineColor;
                lineRenderer.startWidth = lineWidth;
                lineRenderer.endWidth = lineWidth;
                lineRenderer.positionCount = 2;
                Vector3 p0 = new Vector3(bounds.bounds.min.x, y, z);
                Vector3 p1 = new Vector3(bounds.bounds.max.x, y, z);
                lineRenderer.SetPosition(0, p0);
                lineRenderer.SetPosition(1, p1);
            }
        }
        for (float x = bounds.bounds.min.x; x <= bounds.bounds.max.x; x += helper.transform.localScale.x)
        {
            for (float z = bounds.bounds.min.z; z <= bounds.bounds.max.z; z += helper.transform.localScale.z)
            {
                GameObject line = new GameObject("Line" + linesCount++, typeof(LineRenderer));
                line.transform.parent = transform;
                LineRenderer lineRenderer = line.GetComponent<LineRenderer>();
                lineRenderer.useWorldSpace = true;
                lineRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                lineRenderer.material = new Material(Shader.Find("Legacy Shaders/Particles/Additive"));
                // update line renderer
                lineRenderer.startColor = lineColor;
                lineRenderer.endColor = lineColor;
                lineRenderer.startWidth = lineWidth;
                lineRenderer.endWidth = lineWidth;
                lineRenderer.positionCount = 2;
                Vector3 p0 = new Vector3(x, bounds.bounds.min.y, z);
                Vector3 p1 = new Vector3(x, bounds.bounds.max.y, z);
                lineRenderer.SetPosition(0, p0);
                lineRenderer.SetPosition(1, p1);
            }
        }*/
        StartCoroutine(Check());
    }

    IEnumerator Check()
    {
        all = 0;
        both = 0;
        for (float z = bounds.bounds.min.z; z <= bounds.bounds.max.z; z += helper.transform.localScale.z)
        {
            foreach (Transform t in helpers) t.position = new Vector3(t.position.x, t.position.y, z);
            all += helpers.Count;
            yield return new WaitForFixedUpdate();
        }
        //Debug.Log((float)both / all);
        score = (float)both / all;
        StartCoroutine(Check());
    }
}
