﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[RequireComponent(typeof(AudioSource))]
[ExecuteInEditMode]
public class CutScript : MonoBehaviour
{
    [SerializeField] LayerMask mask;
    [SerializeField] Vector3 blade;
    RaycastHit hit;
    bool cutOrder;
    float progress = 0f;
    [SerializeField] float cutLimit = 20f, cutDistance = 1f;
    Vector3 oldPos;
    AudioSource audio;
    [SerializeField] int woodLayer, stoneLayer, metalLayer;
    [SerializeField] AudioClip woodSound, stoneSound, metalSound;
    [SerializeField] Material woodMaterial, stoneMaterial, metalMaterial;
    void Start()
    {
        oldPos = transform.position;
        audio = GetComponent<AudioSource>();
        audio.loop = true;
        audio.spatialBlend = 1;
        audio.playOnAwake = false;
        if (audio.isPlaying) audio.Stop();
    }

    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.A)) cutOrder = true;
        if (transform.parent && Physics.BoxCast(transform.position, blade, transform.forward, out hit, Quaternion.identity, cutDistance, mask))
        {
            float deltaProgress = Mathf.Abs((transform.position - oldPos).magnitude);
            progress += deltaProgress;
            if (hit.transform.gameObject.layer == woodLayer) audio.clip = woodSound;
            if (hit.transform.gameObject.layer == stoneLayer) audio.clip = stoneSound;
            if (hit.transform.gameObject.layer == metalLayer) audio.clip = metalSound;
            if (deltaProgress > 0 && !audio.isPlaying) audio.Play();
            else if (deltaProgress == 0) audio.Pause();
            oldPos = transform.position;
            if (progress >= cutLimit)
            {
                progress %= cutLimit;
                cutOrder = true;
            }
            if (cutOrder)
            {
                GameObject target = hit.collider.gameObject;
                Material cutMat;
                if (hit.transform.gameObject.layer == woodLayer) cutMat = woodMaterial;
                else if (hit.transform.gameObject.layer == stoneLayer) cutMat = stoneMaterial;
                else cutMat = metalMaterial;
                if (!target || !target.GetComponent<MeshFilter>() || !target.GetComponent<MeshFilter>().mesh) return;
                GameObject[] parts = MeshCut.Cut(target, transform.position, transform.right, cutMat);
                if (parts.Length > 1)
                {
                    GameObject g = parts[1];
                    if (!g.GetComponent<Rigidbody>()) g.AddComponent<Rigidbody>();
                    if (!g.GetComponent<MeshCollider>()) g.AddComponent<MeshCollider>();
                    g.GetComponent<MeshCollider>().sharedMesh = g.GetComponent<MeshFilter>().sharedMesh;
                    g.GetComponent<MeshCollider>().convex = true;
                    if (!g.GetComponent<Interactable>()) g.AddComponent<Interactable>();
                    if (!g.GetComponent<Throwable>()) g.AddComponent<Throwable>();
                    if (!g.GetComponent<CustomObjectScript>()) g.AddComponent<CustomObjectScript>();
                    if (parts[0].GetComponent<Rigidbody>().isKinematic) g.GetComponent<Rigidbody>().isKinematic = true;
                    if (parts[0].transform.parent && parts[0].transform.parent.GetComponent<CustomObjectScript>()) g.transform.parent = parts[0].transform.parent;
                    else g.transform.parent = null;
                    g.transform.position = new Vector3(g.transform.position.x - g.transform.position.x % 0.01f,
                        g.transform.position.y - g.transform.position.y % 0.01f,
                        g.transform.position.z - g.transform.position.z % 0.01f);
                }
                cutOrder = false;
            }
        }
        else progress = 0;
        Debug.DrawRay(transform.position, transform.forward, Color.green);
        Debug.DrawRay(transform.position + new Vector3(blade.x, 0, 0), transform.forward, Color.green);
        Debug.DrawRay(transform.position - new Vector3(blade.x, 0, 0), transform.forward, Color.green);
    }
}
