﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomObjectScript : MonoBehaviour
{
    Transform oldParent = null;
    public bool attached { get; private set; }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x - transform.position.x % 0.01f,
                transform.position.y - transform.position.y % 0.01f,
                transform.position.z - transform.position.z % 0.01f);
        //transform.localScale = new Vector3(transform.localScale.x - transform.localScale.x % 0.01f,
        //        transform.localScale.y - transform.localScale.y % 0.01f,
        //        transform.localScale.z - transform.localScale.z % 0.01f);
    }

    public void AttachTo(Transform parent)
    {
        transform.parent = parent;
        if (parent == null) attached = false;
        else attached = true;
    }
}
